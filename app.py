from flask import Flask, render_template, request, session, abort, flash, redirect, url_for, g
from itsdangerous import URLSafeSerializer
import rethinkdb as r
from rethinkdb.errors import RqlRuntimeError, RqlDriverError
import json
from werkzeug.security import generate_password_hash, check_password_hash
import base64

# config app
app = Flask(__name__)
app.secret_key = 'semewew'

# secret key URL Safe Serializer Its Dangerous
global s
s = URLSafeSerializer('semewew')

# config database
RDB_HOST =  'localhost'
RDB_PORT = 28015
DB = 'paw'

def dbSetup():
    connection = r.connect(host=RDB_HOST, port=RDB_PORT)
    try:
        r.db_create(DB).run(connection)
        r.db(DB).table_create('users').run(connection)
        r.db(DB).table_create('books').run(connection)
        print 'Database setup completed. Now run the app without --setup.'
    except RqlRuntimeError:
        print 'App database already exists. Run the app without --setup.'
    finally:
        connection.close()

# open connection before each request
@app.before_request
def before_request():
	try:
		g.rdb_conn = r.connect(host=RDB_HOST, port=RDB_PORT, db=DB)
	except RqlDriverError:
		abort(503, "Database connection could be established.")

# close the connection after each request
@app.teardown_request
def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
		pass

@app.route('/')
def home():
	books = list(r.table('books').run(g.rdb_conn))
	for book in books:
		book['id'] = s.dumps(book['id']) # dump URL using URLsafeserializer Its Dangerous
	return render_template('home.html', books = books)

@app.route('/login', methods=["GET", "POST"])
def login():
	if request.method == 'POST':
		email = request.form['email']
		password = request.form['password']
		user = list(r.table('users').filter({'email': email}).run(g.rdb_conn))
		cek_pass = check_password_hash(user[0]['password'], password)
		if cek_pass:
			session['email'] = email
			session['user'] = user[0]['id']
			return redirect(url_for('home'))
		else:
			flash('Password salah')
			return redirect(url_for('login'))
	else:
		return render_template('login.html')

@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for('home'))

@app.route('/register', methods=['GET','POST'])
def register():
	if request.method == 'POST':
		email = request.form['email']
		password = request.form['password']
		password2 = request.form['password-repeat']
		contact = request.form['contact']
		city = request.form['city']
		if password == password2:
			password = generate_password_hash(password)
			r.table('users').insert({'email': email, 'password': password, 'contact': contact, 'city': city}).run(g.rdb_conn)
			return home()
		else:
			flash('Password tidak cocok')
			return redirect(url_for('register'))
	else:
		return render_template('register.html')

@app.route('/add-product', methods=['GET','POST'])
def addproduct():
	if request.method == 'POST':
		judul = request.form['judul']
		foto = request.files['foto']
		id_owner = session['user']
		foto_str = base64.b64encode(foto.read()) #base64 encoding
		r.table('books').insert({'title': judul, 'id_owner': id_owner,'image': foto_str, }).run(g.rdb_conn)
		return redirect(url_for('home'))
	else:
		return render_template('add-product.html')

@app.route('/product-list')
def productlist():
	return render_template('product-list.html')

@app.route('/detail/<idbook>')
def detail(idbook):
	idbook = s.loads(idbook) 
	book = r.table('books').get(idbook).run(g.rdb_conn)
	owner = r.table('users').get(book['id_owner']).run(g.rdb_conn)
	return render_template('detail.html', book = book, owner = owner)

@app.route('/category/<category>')
def category(category):
	books = r.table('books').filter({'category': category}).run(g.rdb_conn)
	return render_template('category.html', books = books)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run the Flask todo app')
    parser.add_argument('--setup', dest='run_setup', action='store_true')
    args = parser.parse_args()
    if args.run_setup:
        dbSetup()
    else:
        app.run(debug=True)